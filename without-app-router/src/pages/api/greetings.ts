// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import { IUser } from "@/api/types";
import type { NextApiRequest, NextApiResponse } from "next";

export default function handler(
  _req: NextApiRequest,
  res: NextApiResponse<IUser>
) {
  res.status(200).json({ name: "Veiko" });
}
