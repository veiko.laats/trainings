import Head from "next/head";
import { getUser } from "@/api/get-user";
import styles from "@/styles/Home.module.scss";

const AboutPage: React.FC = () => (
  <>
    <Head>
      <title>NextJS Demo Without App router - About</title>
      <meta
        name="description"
        content="NextJS Demo Without App router - About"
      />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <link rel="icon" href="/favicon.ico" />
    </Head>
    <div className={styles.wrapper}>
      <h2>About</h2>
      <div className={styles["body-container"]}>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto,
          quam pariatur id, debitis fuga est sit rem distinctio voluptas eaque
          cumque ducimus dolor alias ipsam, iure dignissimos enim laudantium ad.
          Lorem ipsum dolor sit amet, consectetur adipisicing elit.
        </p>
      </div>
    </div>
  </>
);

export async function getServerSideProps() {
  const user = await getUser();
  return { props: { user } };
}

export default AboutPage;
