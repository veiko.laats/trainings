import "@/styles/globals.css";
import type { AppProps } from "next/app";
import { Inter } from "next/font/google";
import Header from "@/components/Header";

const inter = Inter({ subsets: ["latin"] });

const App = ({ Component, pageProps }: AppProps): JSX.Element => (
  <div className={inter.className}>
    <Header name={pageProps.user.name} />
    <main>
      <Component {...pageProps} />
    </main>
  </div>
);

export default App;
