import Link from "next/link";
import { IUser } from "@/api/types";
import styles from "./Header.module.scss";

const Header: React.FC<IUser> = ({ name }) => (
  <header className={styles.header}>
    <div>
      <Link className={styles["header-logo"]} href="/">
        ▲
      </Link>
      <Link className={styles["header-navigation-link"]} href="/about">
        About
      </Link>
    </div>
    <span className={styles["header-auth-user"]}>Welcome, {name}!</span>
  </header>
);

export default Header;
