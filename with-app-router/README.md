# NextJS Demo With App router

## 1. Requirements

- Node.js >= 18.16 ([nvm](https://github.com/nvm-sh/nvm) or <https://nodejs.org/en/download/>)
- Git >= v2.23.0 (<https://git-scm.com/>)

## 2. Local installation

### 2.1. Setup environment variables

Copy `example.env` in the project root to `.env.local` and edit your preferences.

Example:

```properties
NEXT_PUBLIC_BACKEND_BASE_PATH=http://localhost:3001
NEXT_PUBLIC_APP_BASE_PATH=http://localhost:3000
```

### 2.2. Install dependencies

`npm install`

### 2.3. Run local mock db server

`npm run mock-db:start`

### 2.4. Run dev build

`npm run dev`

## 3. Run production build

`npm run build`

`npm run start`