import { getPost } from "@/api/get-post";
import type { IPagePathParams } from "../../types";
import PostContent from "../../components/PostContent";

export const generateMetadata = async ({
  params,
}: {
  params: IPagePathParams;
}) => {
  const post = await getPost(params.id);
  return {
    title: `NextJS Demo - Post ${post.id}`,
    description: "NextJS Demo With App router",
  };
};

const PostPage: React.FC<{ params: IPagePathParams }> = async ({ params }) => {
  const post = await getPost(params.id);
  return <PostContent post={post} />;
};

export default PostPage;
