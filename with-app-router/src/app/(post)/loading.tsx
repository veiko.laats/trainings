"use client";
import { themeTss } from "@/common/theme/themeTss";

const useStyles = themeTss.create(({ spacing }) => ({
  container: {
    padding: spacing.xl,
  },
}));

const Loading: React.FC = () => {
  const { classes } = useStyles();
  return (
    <div className={classes.container}>
      <p>Loading...</p>
    </div>
  );
};

export default Loading;
