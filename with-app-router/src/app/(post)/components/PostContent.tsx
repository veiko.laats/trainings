"use client";
import { memo } from "react";
import type { IPost } from "@/api/types";
import { themeTss } from "@/common/theme/themeTss";

const useStyles = themeTss.create(({ spacing, color, font }) => ({
  container: {
    padding: spacing.xl,
  },
  title: {
    marginBottom: spacing.l,
  },
  content: {
    marginBottom: spacing.l,
  },
}));

const PostContent: React.FC<{ post: IPost }> = ({ post }) => {
  const { classes } = useStyles();
  return (
    <div className={classes.container}>
      <h2 className={classes.title}>{post.title}</h2>
      <p className={classes.content}>{post.body}</p>
    </div>
  );
};

export default memo(PostContent);
