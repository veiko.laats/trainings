import { Metadata } from "next";
import HomeContent from "./components/HomeContent";

export const metadata: Metadata = {
  title: "NextJS Demo - Home",
  description: "NextJS Demo With App router",
};

const HomePage: React.FC = () => <HomeContent />;

export default HomePage;
