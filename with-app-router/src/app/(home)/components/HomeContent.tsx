"use client";
import { themeTss } from "@/common/theme/themeTss";

const useStyles = themeTss.create(({ spacing }) => ({
  wrapper: {
    padding: spacing.xl,
  },
  bodyContainer: {
    marginTop: spacing.m,
  },
}));

const HomeContent: React.FC = () => {
  const { classes } = useStyles();
  return (
    <div className={classes.wrapper}>
      <h2>Home</h2>
      <div className={classes.bodyContainer}>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto,
          quam pariatur id, debitis fuga est sit rem distinctio voluptas eaque
          cumque ducimus dolor alias ipsam, iure dignissimos enim laudantium ad.
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto,
          quam pariatur id, debitis fuga est sit rem distinctio voluptas eaque
          cumque ducimus dolor alias ipsam, iure dignissimos enim laudantium ad.
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto,
          quam pariatur id, debitis fuga est sit rem distinctio voluptas eaque
          cumque ducimus dolor alias ipsam, iure dignissimos enim laudantium ad.
        </p>
      </div>
    </div>
  );
};

export default HomeContent;
