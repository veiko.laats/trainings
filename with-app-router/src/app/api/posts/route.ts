import { NextResponse } from "next/server";
import type { IPost } from "@/api/types";
import { getBackendPath } from "@/api/utils";
import { ApiPath } from "@/api/ApiPath";

export const GET = async () => {
  const res = await fetch(getBackendPath(ApiPath.PostsApi.getPosts));
  const data: IPost[] = await res.json();

  return NextResponse.json([...data]);
};
