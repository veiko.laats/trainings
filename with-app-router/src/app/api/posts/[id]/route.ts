import { NextRequest, NextResponse } from "next/server";
import type { IPost } from "@/api/types";
import type { IPagePathParams } from "@/app/(post)/types";
import { getBackendPath } from "@/api/utils";
import { ApiPath } from "@/api/ApiPath";

export const GET = async (
  _req: NextRequest,
  { params }: { params: IPagePathParams }
) => {
  const res = await fetch(getBackendPath(ApiPath.PostsApi.getPost(params.id)));
  const data: IPost = await res.json();

  return NextResponse.json({ ...data });
};
