import { NextResponse } from "next/server";
import type { IUser } from "@/api/types";
import { getBackendPath } from "@/api/utils";
import { ApiPath } from "@/api/ApiPath";

export const GET = async () => {
  const res = await fetch(getBackendPath(ApiPath.UsersApi.getUser));
  const data: IUser = await res.json();

  return NextResponse.json({ name: data.name });
};
