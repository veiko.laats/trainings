"use client";
import Skeleton from "react-loading-skeleton";
import { themeTss } from "@/common/theme/themeTss";
import "react-loading-skeleton/dist/skeleton.css";

const useStyles = themeTss.create(({ spacing, color }) => ({
  skeletonContainer: {
    padding: spacing.xl,
  },
  skeletonTitle: {
    marginBottom: spacing.l,
  },
  skeletonItemContainer: {
    padding: spacing.xl,
    border: 1,
    borderStyle: "solid",
    borderColor: color.white,
    marginBottom: spacing.m,
  },
  skeletonItemTitle: {
    marginBottom: spacing.m,
  },
}));

const Loading: React.FC = () => {
  const { classes } = useStyles();
  return (
    <div className={classes.skeletonContainer}>
      <h2 className={classes.skeletonTitle}>Posts</h2>
      <ul>
        {[...new Array(4)].map((_, idx) => (
          <div key={idx} className={classes.skeletonItemContainer}>
            <Skeleton
              height={22}
              width="50%"
              className={classes.skeletonItemTitle}
            />
            <Skeleton height={25} />
          </div>
        ))}
      </ul>
    </div>
  );
};

export default Loading;
