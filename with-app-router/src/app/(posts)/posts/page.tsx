import { Metadata } from "next";
import { getPosts } from "@/api/get-posts";
import PostsContent from "./components/PostsContents";

export const metadata: Metadata = {
  title: "NextJS Demo - Posts",
  description: "NextJS Demo With App router",
};

const PostsPage: React.FC = async () => {
  const posts = await getPosts();
  return <PostsContent posts={posts} />;
};

export default PostsPage;
