import Link from "next/link";
import { themeTss } from "@/common/theme/themeTss";
import type { IPost } from "@/api/types";

const useStyles = themeTss.create(({ spacing, color }) => ({
  container: {
    padding: spacing.xl,
    border: 1,
    borderStyle: "solid",
    borderColor: color.white,
    marginBottom: spacing.m,
  },
  title: {
    marginBottom: spacing.m,
  },
}));

const PostItem: React.FC<{ post: IPost }> = ({ post }) => {
  const { classes } = useStyles();
  return (
    <Link href={`/posts/${post.id}`}>
      <div key={post.id} className={classes.container}>
        <h3 className={classes.title}>{post.title}</h3>
        <p>{post.body}</p>
      </div>
    </Link>
  );
};

export default PostItem;
