"use client";
import { memo } from "react";
import { themeTss } from "@/common/theme/themeTss";
import type { IPost } from "@/api/types";
import PostItem from "./PostItem";

const useStyles = themeTss.create(({ spacing }) => ({
  container: {
    padding: spacing.xl,
  },
  title: {
    marginBottom: spacing.l,
  },
}));

const PostsContent: React.FC<{ posts: IPost[] }> = ({ posts }) => {
  const { classes } = useStyles();
  return (
    <div className={classes.container}>
      <h2 className={classes.title}>Posts</h2>
      <ul>
        {posts && posts.map((post) => <PostItem post={post} key={post.id} />)}
      </ul>
    </div>
  );
};

export default memo(PostsContent);
