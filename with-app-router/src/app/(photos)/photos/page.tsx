import { Metadata } from "next";
import PhotosContent from "./components/PhotosContent";

export const metadata: Metadata = {
  title: "NextJS Demo - Photos",
  description: "NextJS Demo With App router",
};

const PhotosPage: React.FC = () => <PhotosContent />;

export default PhotosPage;
