"use client";
import React from "react";
import Skeleton from "react-loading-skeleton";
import { themeTss } from "@/common/theme/themeTss";
import "react-loading-skeleton/dist/skeleton.css";

const useStyles = themeTss.create(({ spacing, color }) => ({
  container: {
    display: "flex",
    alignItems: "center",
    padding: spacing.xl,
    border: 1,
    borderStyle: "solid",
    borderColor: color.white,
    marginBottom: spacing.m,
  },
  photo: {
    marginRight: spacing.l,
  },
  title: {
    marginBottom: spacing.m,
  },
}));

const PhotosLoading: React.FC = () => {
  const { classes } = useStyles();
  return (
    <ul>
      {[...new Array(4)].map((_, idx) => (
        <div key={idx} className={classes.container}>
          <Skeleton height={100} width={100} className={classes.photo} />
          <Skeleton height={20} width={500} />
        </div>
      ))}
    </ul>
  );
};

export default PhotosLoading;
