import { getPhotos } from "@/api/get-photos";
import PhotosListItem from "./PhotosListItem";

const PhotosList: React.FC = async () => {
  const photos = await getPhotos();
  return (
    <ul>
      {photos?.map((photo) => (
        <PhotosListItem photo={photo} key={photo.id} />
      ))}
    </ul>
  );
};

export default PhotosList;
