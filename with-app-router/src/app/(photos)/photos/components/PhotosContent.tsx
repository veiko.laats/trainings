"use client";
import { Suspense, useState } from "react";
import { themeTss } from "@/common/theme/themeTss";
import PhotosList from "./PhotosList";
import PhotosLoading from "./PhotosLoading";

const useStyles = themeTss.create(({ spacing, color, font }) => ({
  wrapper: {
    padding: spacing.xl,
  },
  bodyContainer: {
    marginTop: spacing.m,
  },
  photosContainer: {
    marginTop: spacing.l,
  },
  loadPhotosButton: {
    border: "none",
    outline: "none",
    userSelect: "none",
    paddingTop: spacing.m,
    paddingBottom: spacing.m,
    paddingLeft: spacing.l,
    paddingRight: spacing.l,
    backgroundColor: color.white,
    color: color.black,
    fontSize: font.size.s,
    cursor: "pointer",
  },
}));

const PhotosContent: React.FC = () => {
  const { classes } = useStyles();
  const [shouldLoadPhotos, setLoadPhotos] = useState(false);

  const toggleLoadPhotos = () => {
    setLoadPhotos((prev) => !prev);
  };

  return (
    <div className={classes.wrapper}>
      <h2>Photos</h2>
      <div className={classes.bodyContainer}>
        <button className={classes.loadPhotosButton} onClick={toggleLoadPhotos}>
          {shouldLoadPhotos ? "Hide photos" : "Load photos"}
        </button>
        <div className={classes.photosContainer}>
          {shouldLoadPhotos && (
            <Suspense fallback={<PhotosLoading />}>
              <PhotosList />
            </Suspense>
          )}
        </div>
      </div>
    </div>
  );
};

export default PhotosContent;
