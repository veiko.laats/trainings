import { memo } from "react";
import Image from "next/image";
import { themeTss } from "@/common/theme/themeTss";
import type { IPhoto } from "@/api/types";

const useStyles = themeTss.create(({ spacing, color }) => ({
  container: {
    display: "flex",
    alignItems: "center",
    padding: spacing.xl,
    border: 1,
    borderStyle: "solid",
    borderColor: color.white,
    marginBottom: spacing.m,
  },
  photo: {
    marginRight: spacing.l,
  },
  title: {
    marginBottom: spacing.m,
  },
}));

const PhotosListItem: React.FC<{ photo: IPhoto }> = ({ photo }) => {
  const { classes } = useStyles();
  return (
    <div className={classes.container}>
      <Image
        className={classes.photo}
        src={photo.url}
        alt={photo.title}
        width={100}
        height={100}
      />
      <div>{photo.title}</div>
    </div>
  );
};

export default memo(PhotosListItem);
