import "./globals.css";
import { NextAppDirEmotionCacheProvider } from "tss-react/next/appDir";
import { Inter } from "next/font/google";
import Header from "@/common/components/Header";

const inter = Inter({ subsets: ["latin"] });

const RootLayout: React.FC<{ children: React.ReactNode }> = ({ children }) => (
  <html lang="en">
    <body className={inter.className}>
      <NextAppDirEmotionCacheProvider options={{ key: "custom" }}>
        <Header />
        <main>{children}</main>
      </NextAppDirEmotionCacheProvider>
    </body>
  </html>
);

export default RootLayout;
