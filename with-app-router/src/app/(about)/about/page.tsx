import { Metadata } from "next";
import AboutContent from "../components/AboutContent";

export const metadata: Metadata = {
  title: "NextJS Demo - About",
  description: "NextJS Demo With App router",
};

const AboutPage: React.FC = () => <AboutContent />;

export default AboutPage;
