import type { IPost } from "./types";
import { getNextAppPath, timeOutMs } from "./utils";
import { NextApiPath } from "./ApiPath";

export const getPost = async (id: string): Promise<IPost> => {
  const response = await fetch(
    getNextAppPath(NextApiPath.PostsApi.getPost(id)),
    {
      cache: "no-store",
    }
  );
  await timeOutMs(2000);
  const data: IPost = await response.json();
  return data;
};
