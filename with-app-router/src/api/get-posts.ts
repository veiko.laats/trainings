import type { IPost } from "./types";
import { getNextAppPath, timeOutMs } from "./utils";
import { NextApiPath } from "./ApiPath";

export const getPosts = async (): Promise<IPost[]> => {
  const response = await fetch(getNextAppPath(NextApiPath.PostsApi.getPosts), {
    cache: "no-store",
  });
  await timeOutMs();
  const data: IPost[] = await response.json();
  return data;
};
