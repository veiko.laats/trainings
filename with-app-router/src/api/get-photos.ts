import type { IPhoto } from "./types";
import { getBackendPath, timeOutMs } from "./utils";
import { ApiPath } from "./ApiPath";

export const getPhotos = async (): Promise<IPhoto[]> => {
  const response = await fetch(getBackendPath(ApiPath.PhotosApi.getPhotos), {
    cache: "no-store",
  });
  await timeOutMs(1000);
  const data: IPhoto[] = await response.json();
  return data;
};
