export interface IUser {
  name: string;
}

export interface IPost {
  id: number;
  title: string;
  body: string;
}

export interface IPhoto {
  id: number;
  title: string;
  url: string;
}
