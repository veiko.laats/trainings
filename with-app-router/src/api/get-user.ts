import { NextApiPath } from "./ApiPath";
import type { IUser } from "./types";
import { getNextAppPath } from "./utils";

export const getUser = async (): Promise<IUser> => {
  const response = await fetch(getNextAppPath(NextApiPath.UsersApi.getUser), {
    cache: "no-store",
  });
  const data: IUser = await response.json();
  return data;
};
