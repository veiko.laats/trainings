export const getBackendPath = (
  url: string,
  basePath = process.env.NEXT_PUBLIC_BACKEND_BASE_PATH
) => `${basePath}${url}`;

export const getNextAppPath = (
  url: string,
  basePath = process.env.NEXT_PUBLIC_APP_BASE_PATH
) => `${basePath}${url}`;

export const timeOutMs = async (time = 3000) => {
  await new Promise((resolve) => setTimeout(resolve, time));
};
