const ApiPath = {
  PostsApi: {
    getPosts: "/posts",
    getPost: (postId: string) => `/posts/${postId}`,
  },
  UsersApi: {
    getUser: "/user",
  },
  PhotosApi: {
    getPhotos: "/photos",
  },
};

const NextApiPath = {
  PostsApi: {
    getPosts: "/api/posts",
    getPost: (postId: string) => `/api/posts/${postId}`,
  },
  UsersApi: {
    getUser: "/api/greetings",
  },
  PhotosApi: {
    getPhotos: "/api/photos",
  },
};

export { ApiPath, NextApiPath };
