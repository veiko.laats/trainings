const theme = {
  color: {
    white: "#FFFFFF",
    black: "#000000",
  },
  spacing: {
    xxs: 2,
    xs: 4,
    s: 8,
    m: 16,
    l: 24,
    xl: 32,
    xxl: 40,
    "3xl": 48,
    "4xl": 64,
    "5xl": 128,
  },
  font: {
    size: {
      "3xs": 12,
      xxs: 14,
      xs: 15,
      s: 16,
      m: 18,
      l: 24,
      xl: 36,
      xxl: 48,
    },
    lineHeight: {
      s: 1.2,
      m: 1.4,
      l: 1.7,
    },
    family: "Inter",
  },
};

export default theme;
