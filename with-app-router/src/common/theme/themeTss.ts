import { createTss } from "tss-react";
import theme from "./theme";

const useThemeContext = () => {
  return { ...theme };
};

const { tss } = createTss({ useContext: useThemeContext });
export const themeTss = tss;
