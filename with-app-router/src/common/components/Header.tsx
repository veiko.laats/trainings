import { getUser } from "@/api/get-user";
import HeaderContent from "./HeaderContent";

const Header: React.FC = async () => {
  const user = await getUser();
  return <HeaderContent user={user} />;
};

export default Header;
