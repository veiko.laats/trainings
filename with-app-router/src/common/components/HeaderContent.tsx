"use client";
import { memo } from "react";
import Link from "next/link";
import { themeTss } from "@/common/theme/themeTss";
import type { IUser } from "@/api/types";

const useStyles = themeTss.create(({ spacing, font }) => ({
  header: {
    display: "flex",
    padding: spacing.xl,
    justifyContent: "space-between",
    alignItems: "center",
  },
  headerLogo: {
    fontSize: font.size.l,
  },
  headerNavigationLink: {
    fontSize: font.size.s,
    marginLeft: spacing.s,
    "&:hover": {
      textDecoration: "underline",
    },
  },
  headerAuthUser: {
    fontSize: font.size.s,
  },
}));

const HeaderContent: React.FC<{ user: IUser }> = ({ user }) => {
  const { classes } = useStyles();
  return (
    <header className={classes.header}>
      <div>
        <Link href="/" className={classes.headerLogo}>
          ▲
        </Link>
        <Link href="/about" className={classes.headerNavigationLink}>
          About
        </Link>
        <Link href="/posts" className={classes.headerNavigationLink}>
          Posts
        </Link>
        <Link href="/photos" className={classes.headerNavigationLink}>
          Photos
        </Link>
      </div>
      <span className={classes.headerAuthUser}>Welcome, {user.name}!</span>
    </header>
  );
};

export default memo(HeaderContent);
